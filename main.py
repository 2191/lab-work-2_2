m = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
str = 'Music is the soul of languaget'
word = 'lang'


def Search_lin(m, val): # Линейный поиск
    counter = 0
    for i in range(len(m)):
        counter += 1
        if m[i] == val:
            counter += 1
            return [i, counter]
    return [-1, counter]
print(Search_lin(m, 6), ' - Линейный поиск')

def Search_bin(m, val): # Бинарный поиск
    counter = 0
    first = 0
    last = len(m) - 1
    index = -1
    while (first <= last) and (index == -1):
        mid = (first + last) // 2
        counter += 1
        if m[mid] == val:
            index = mid
            counter += 1
        else:
            counter += 1
            if val < m[mid]:
                last = mid - 1
                counter += 1
            else:
                first = mid + 1
                counter += 1
    return [index, counter]

print(Search_bin(m, 6), ' - Бинарный поиск')

def Search_naive(str, val): # Наивный поиск
    counter = 0
    pat_len = len(val)
    position = []
    counter += 1
    for i in range(len(str) - pat_len + 1):
        match_found = True
        counter += 1
        for j in range(pat_len):
            counter += 1
            if str[i + j] != val[j]:
                match_found = False
                counter += 1
                break
        if match_found:
            position.append(i)
            counter += 1
    return position, counter
print(Search_naive(str, word), ' - Наивный поиск')

def Search_kmp(str, val): # Кнут-Моррис-Пратт
    counter = 0
    next = [0]
    j = 0
    for i in range(1, len(val)):
        counter += 1
        while j > 0 and val[j] != val[i]:
            j = next[j - 1]
            counter += 1
        if val[j] == val[i]:
            j += 1
        next.append(j)
        counter += 1
    ans = []
    j = 0
    for i in range(len(str)):
        counter += 1
        while j > 0 and str[i] != val[j]:
            j = next[j - 1]
            counter += 1
        if str[i] == val[j]:
            j += 1
        if j == len(val):
            ans.append(i - (j - 1))
            j = next[j - 1]
            counter += 2
    return ans, counter
print(Search_kmp(str, word), ' -  поиск Кнута-Морриса-Пратта')