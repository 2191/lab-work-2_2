from main import Search_lin, Search_bin, Search_naive, Search_kmp

m = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
str = 'Music is the soul of languaget'
word = 'lang'

def test_Search_lin():
    assert Search_lin(m, 6)[0] == 5

def test_Search_bin():
    assert Search_bin(m, 6)[0] == 5

def test_Search_naive():
    assert Search_naive(str, word)[0] == [21]

def test_Search_kmp():
    assert Search_kmp(str, word)[0] == [21]